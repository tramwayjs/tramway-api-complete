import {commands} from 'tramway-command';
import index from './config/parameters/commands.js';
const {CommandResolver} = commands;

export default (new CommandResolver(index)).run();