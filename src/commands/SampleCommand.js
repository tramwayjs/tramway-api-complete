import {Command, commands} from 'tramway-command';
let {InputOption} = commands;

export default class SampleCommand extends Command {
    configure() {
        this.args.add((new InputOption('input', InputOption.string)).isRequired());
        this.options.add(new InputOption('bool', InputOption.boolean));
    }

    action() {
        console.log("Test command ran, here's some of what you gave it", this.args, this.options);
    }
}
