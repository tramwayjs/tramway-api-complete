import {
    MainController,
} from '../../controllers';

export default {
    "main.controller": {
        "class": MainController,
        "constructor": [
            {"type": "service", "key": "router"}
        ],
    }
}