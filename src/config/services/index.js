import core from './core';
import controllers from './controllers';
import router from './router';
import services from './services';

export default {
    ...core,
    ...controllers,
    ...router,
    // ...services,
}