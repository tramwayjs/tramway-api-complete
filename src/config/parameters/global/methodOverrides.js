import methodOverride from 'method-override';

export const _method = methodOverride('_method');
export const xMethod = methodOverride('X-HTTP-Method-Override');