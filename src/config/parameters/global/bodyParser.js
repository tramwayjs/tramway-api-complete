import bodyParser from 'body-parser';

export const json = bodyParser.json();
export const urlEncoding = bodyParser.urlencoded({ extended: false });