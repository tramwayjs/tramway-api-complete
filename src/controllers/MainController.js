import {Controller} from 'tramway-core-router'; 

export default class MainController extends Controller {
    index(req, res) {
        return res.json({"key": "value"});
    }
}