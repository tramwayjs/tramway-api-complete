"use strict";

const gulp = require('gulp');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const nodemon = require('gulp-nodemon');

const DEV_DIRECTORY = 'src/**/*.js';
const DIST_DIRECTORY = 'dist';
const MAIN_FILE = 'index';

gulp.task('build', function() {
    return gulp
        .src(DEV_DIRECTORY)
        .pipe(sourcemaps.init())
        .pipe(babel({
            "plugins": [
                "transform-flow-strip-types",
                "transform-object-rest-spread",
            ],
            "presets": [
                "es2015-node6"
            ]
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(DIST_DIRECTORY));
});

gulp.task('watch', ['build'], function(){
    gulp.watch(DEV_DIRECTORY, ['build', 'watch']);
});

gulp.task('server', ['watch'], function() {
    return nodemon({script: `./${DIST_DIRECTORY}/${MAIN_FILE}.js`}).on('restart', function () {
        console.log('restarted');
    })
});

gulp.task('default', ['watch']);
