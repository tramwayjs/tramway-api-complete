Tramway API Complete
====================

Tramway API Complete is a bundle of Tramway components that make up the core of an API built in NodeJS using service oriented architecture. It features:
- Dependency Injection for almost all components
- Dynamic routing and Controllers
- Commands
- Flexible Security policies with a wrapper for Auth0
- The ability to make RESTful routes
- Repositories, Services, Factories are implementable
- Providers are implementable, standard ones for interfacing with APIs, MySQL, ArangoDB are available
- Built on ExpressJS
- Uses Mocha for unit tests

Getting Started
---------------

Clone this repository and start developing

## Building the code

```
gulp build
```

## Live development environment

```
gulp server
```

## Running the server

```
node dist/index
```

## Running custom commands

Command configuration is handled in config/parameters/commands.js. The COMMANDNAME maps to a Command class.

```
node dist/command <COMMANDNAME>
```